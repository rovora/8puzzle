# -*- coding: utf-8 -*-
"""


@author: rohan
"""
## Rohan Vora
## AI Prog 1
#10/28/2018

### HOW TO USE THIS PROGRAM

#COMMANDS
# run('a', 'rand')    = runs A* search on a random starting config
# run('a', [8,7,6,5,4,2,1,'b']) runs A* on your custom staring config
# run('bf', 'rand')   = runs Best First search on random config
# run('bf', [8,7,6,4,5,3,2,1,'b'])   = runs Best First search on custom config


#Load the module in a python runtime. Type run('a') to run the A* search with
# a random starting config. Type run('bf') to run Best First with a random
# starting config. 

# To swtich the heuristics, uncomment/comment lines 215-218 to hook up
# the desired heuristic for the model.
import random
import copy
from operator import attrgetter

class path:
    def __init__(self, old_path):
        self.my_path = []
    def add(self, config):
        self.my_path.append(config)
    def display(self):
        for i,v in enumerate(self.my_path):
            print(v)
        
class state:
    def __init__(self, config, old_path):
        self.config = config
        self.hval = 0
        self.path_to_here = path(old_path)
        self.len = 0
        self.fval = 0
    def display(self):
        print("CONFIG:  ", self.config)
        print("HVAL:    ", self.hval)
        print("PATH LEN:    ", self.len)


class frontier:
    def __init__(self):
        self.nodes = []
    def display(self):
        print("FRONTIER:   ")
        for i in self.nodes:
            i.display()
    def branch(self, to_expand):

        chunks = expand(to_expand)
        for i, v in enumerate(chunks):
            self.add_state(v)
            
        list(set(self.nodes))
        self.nodes = [x for x in self.nodes if x.config != to_expand.config]
    
    def add_state(self, to_add):
        self.nodes.append(to_add)
    
    def best_first(self):
        
         best_value = min(self.nodes, key=attrgetter('hval')).hval
         best_node = [x for x in self.nodes if x.hval == best_value]
         return random.choice(best_node)
    
    def a_star(self):
        for i in self.nodes:
            i.fval = i.hval + i.len
        
        best_value = min(self.nodes, key=attrgetter('fval')).fval
        best_node = [x for x in self.nodes if x.fval == best_value]
        return random.choice(best_node)



def run(mod, seq):
     #Initialize 
     if len(seq) == 9:
         start = seq
     else: 
         start = [1,2,3,4,5,6,7,8,"b"]
         random.shuffle(start)
         
     i_state = state(start, start)
     i_state.path_to_here.add(start)
     print("INITIAL STATE:  ")
     i_state.display()
     print('\n')
     
     inversions = solvable(i_state.config)
     if inversions  % 2 == 1:
         print("odd amount of inversions, not solvable! --", inversions)
         return
     
     print(inversions)
     print("Even amount of inversions, it's solvable...")
     print("Running Search.")
     
     
     
     if mod == 'a':

      
        my_frontier = frontier()
        my_frontier.add_state(i_state)
    
        #First Branch
        my_frontier.branch(i_state)
        expanding = my_frontier.a_star()
    
        while (expanding.hval > 0) and (expanding.len < 50):
            expanding = my_frontier.a_star()
            my_frontier.branch(expanding)
         
        
        expanding.path_to_here.display()
        expanding.display()
          
     if mod == "bf":
       
        my_frontier = frontier()
        my_frontier.add_state(i_state)
    
        #First Branch
        my_frontier.branch(i_state)
        print("Frontier 1")
        my_frontier.display()
        expanding = my_frontier.best_first()
    
        while (expanding.hval > 0) and (expanding.len < 60):
            expanding = my_frontier.best_first()
            my_frontier.branch(expanding)
            if expanding.hval < 3:
                expanding.display()
        
        expanding.path_to_here.display()
        
   

    
 

def expand(to_expand):
    b = to_expand.config.index('b')


    chunk = []
    
    if b == 0:
       
      chunk.append(swap(b,1, to_expand))
      chunk.append(swap(b,3, to_expand))
       
    if b == 1:
      
      chunk.append(swap(b,0, to_expand))
      chunk.append(swap(b,4, to_expand))
      chunk.append(swap(b,2, to_expand))
       
    if b == 2:
     
      chunk.append(swap(b,1, to_expand))
      chunk.append(swap(b,5, to_expand))
       
    if b == 3:
   
      chunk.append(swap(b,0, to_expand))
      chunk.append(swap(b,4, to_expand))
      chunk.append(swap(b,6, to_expand))
       
    if b == 4:
       
      chunk.append(swap(b,1, to_expand))
      chunk.append(swap(b,3, to_expand))
      chunk.append(swap(b,7, to_expand))
      chunk.append(swap(b,5, to_expand))
       
    if b == 5:
       
      chunk.append(swap(b,2, to_expand))
      chunk.append(swap(b,4, to_expand))
      chunk.append(swap(b,8, to_expand))
       
    if b == 6:
       
      chunk.append(swap(b,7, to_expand))
      chunk.append(swap(b,3, to_expand))
       
    if b == 7:
      
      chunk.append(swap(b,4, to_expand))
      chunk.append(swap(b,6, to_expand))
      chunk.append(swap(b,8, to_expand))
       
    if b == 8:
      
      chunk.append(swap(b,7, to_expand))
      chunk.append(swap(b,5, to_expand))
      

      
    return chunk
      
def solvable(config):
    inversions = 0
    print("Inversion table")
    print("Value, Value : Index, Index")
    for i,v in enumerate(config):
        if v != 'b':
            for k, j in enumerate(config):
                if j != 'b':
                    if v > j and i < k:
                       inversions = inversions + 1
                       print(v,"     ",j,"    ", i,"    ",k)
    return inversions    
                
def swap(b, t, current_state):
    
    move = copy.deepcopy(current_state.config) 
    move[b], move[t] = move[t], move[b]
    answer = state(move, current_state.path_to_here.my_path)
    for i in current_state.path_to_here.my_path:
        answer.path_to_here.add(i)
    answer.path_to_here.add(move)
    answer.len = current_state.len + 1
    
   #Switch around Heuristics here
    #answer.hval = h_tile(answer.config)
    answer.hval = h_man(answer.config)
    #answer.hval = h_weighted_man(answer.config)
    return answer
    

def h_weighted_man(config):
    count = 0
    for (i,v)  in enumerate (config):
        base = convert(i+1)
        if v != 'b':
            tar = convert(v)
            count = count + 2*abs(base[0] - tar[0]) + abs(base[1] - tar[1])
    return count
    
def h_man(config):
    count = 0
    for (i,v)  in enumerate (config):
        base = convert(i+1)
        if v != 'b':
            tar = convert(v)
            count = count + abs(base[0] - tar[0]) + abs(base[1] - tar[1])
    return count


def convert(i):
    
    if i == 1:
        return [1,1]
    if i == 2:
        return [1,2]
    if i == 3:
        return [1,3]
    if i == 4:
        return [2,1]
    if i == 5:
        return [2,2]
    if i == 6:
        return [2,3]
    if i == 7:
        return [3,1]
    if i == 8:
        return [3,2]
    if i == 9:
        return [3,3]
    
def h_tile(config):
    count = 0
    for i, val in enumerate(config):
        if i+1 == val:
            count = count + 1
    
    return 8-count
    